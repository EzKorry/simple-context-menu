import { Component, inject, InjectionKey, reactive, Ref } from "vue";

export interface ContextMenuAPIType {
  state: Ref<"show" | "hide">;
  close(): void;
  itemAs: string | Component;
  itemClass?: string | string[];
  itemStyle?: string | Record<string, unknown>;
}
export interface ContextMenuStoreItem {
  el: HTMLElement;
  handler: (evt: Event) => void;
}

export const ContextMenuAPIKey = Symbol(
  "MyContextMenuAPI"
) as InjectionKey<ContextMenuAPIType>;

export const contextmenuEventHandlerMap = reactive(
  new Map<string, (evt: Event) => void>()
);

export const handlerMap = reactive(new Map<string, (evt: Event) => void>());

export const elementMap = reactive(new Map<string, HTMLElement>());

export function useContextMenuAPI(injectingComponent: string) {
  let context = inject(ContextMenuAPIKey, null);
  if (context === null) {
    let err = new Error(
      `<${injectingComponent} /> is missing a parent <Dialog /> component.`
    );
    throw err;
  }
  return context;
}

const clickOutsideEventlistenerMap = new Map<
  HTMLElement,
  (evt: Event) => void
>();

export function addClickOutsideListener(
  el: HTMLElement,
  handler: (evt: MouseEvent) => void
): void {
  const clickHandler = (evt: Event) => {
    let targetElement: null | HTMLElement = evt.target as HTMLElement; // clicked element

    do {
      if (targetElement === el) {
        // This is a click inside. Do nothing, just return.
        return;
      }
      // Go up the DOM
      targetElement = targetElement?.parentElement;
    } while (targetElement);

    // This is a click outside.
    handler(evt as MouseEvent);
  };
  document.addEventListener("click", clickHandler);
  clickOutsideEventlistenerMap.set(el, clickHandler);
}

export function removeClickOutsideListener(el: HTMLElement): void {
  const clickHandler = clickOutsideEventlistenerMap.get(el);
  if (!clickHandler) return;
  document.removeEventListener("click", clickHandler);
  clickOutsideEventlistenerMap.delete(el);
}
