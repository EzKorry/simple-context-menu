import { Directive, DirectiveBinding, isRef } from "vue";
import {
  contextmenuEventHandlerMap,
  elementMap,
  handlerMap,
} from "./functions";

interface ContextmenuDirectiveEl extends HTMLElement {
  $contextmenuKey?: string;
}

const createHandler = (id: string) => {
  return (evt: Event) => {
    evt.preventDefault();
    const realHandler = handlerMap.get(id);
    if (!realHandler) {
      console.error("real Handler 를 찾을 수 없습니다.");
      return;
    }
    realHandler(evt);
  };
};

const bind = (el: ContextmenuDirectiveEl, binding: DirectiveBinding): void => {
  const id = binding.arg;

  if (!id) {
    console.error("contextmenu Key 가 제공되지 않았습니다.");
    return;
  }

  el.$contextmenuKey = id;
  const handler = createHandler(id);
  elementMap.set(id, el);
  contextmenuEventHandlerMap.set(id, handler);
  el.addEventListener("contextmenu", handler, { capture: true });
};

const unbind = (
  el: ContextmenuDirectiveEl,
  binding: DirectiveBinding
): void => {
  const id = el.$contextmenuKey;

  if (!id) return;

  elementMap.delete(id);
  const handler = contextmenuEventHandlerMap.get(id);
  if (typeof handler === "function") {
    el.removeEventListener("contextmenu", handler, { capture: true });
  }
  contextmenuEventHandlerMap.delete(id);
};

const rebind = (
  el: ContextmenuDirectiveEl,
  binding: DirectiveBinding
): void => {
  unbind(el, binding);
  bind(el, binding);
};

const contextmenuDirective: Directive<ContextmenuDirectiveEl> = {
  mounted: bind,
  updated: rebind,
  beforeUnmount: unbind,
};

export default contextmenuDirective;
