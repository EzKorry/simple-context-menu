import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

export default ({ mode }) => {
  console.log(mode);
  return defineConfig({
    plugins: [vue()],
    base: mode === "production" ? "/simple-context-menu/" : "/",
  });
};
